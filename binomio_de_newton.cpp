#include <iostream>
using namespace std;
/*
* Criado por Gabriel Schneider
* Recife, Agosto 2017
*/

void printar(int n);
int binomio(int linha, int pos);

int main() {
    printar(10);
	return 0;
}

int binomio(int linha, int pos){
    if(pos == 0 || pos == linha-2){
        return 1;
    }
    else {
        int prev = binomio(linha-1,pos-1);
        return (pos+prev);
    }
}

void printar(int n){
    if(n != 0){
        printar(n-1);
        for(int i = 0; i < n; i++){
            if(i != n -1){
	           cout<<binomio(n,i)<<" ";
            }
            else{
            cout<<"\n";
            }
        }
    }
}
